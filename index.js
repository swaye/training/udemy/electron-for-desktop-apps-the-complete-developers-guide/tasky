const path = require('path');
const electron = require('electron');
const TimerTray = require('./app/TimerTray'); 
const TimerWindow = require('./app/MainWindow'); 
const { app, ipcMain } = electron;

let mainWindow;
let tray;
app.on('ready', () => {
  mainWindow = new TimerWindow(`http://localhost:3000/`);
  tray = new TimerTray({ icon: icon(), mainWindow });

  // if the platform is windows, disable the default app in the taskbar
  // else disable in the dock
  isWin32() 
    ? mainWindow.setSkipTaskbar(true)
    : app.dock.hide();  
})

function icon() {
  const iconName = isWin32() 
  ? 'windows-icon@2x.png'
  : 'iconTemplate.png';
  return path.join(__dirname, `./assets/${iconName}`);
}

function isWin32() {
  return process.platform === 'win32';
}

ipcMain.on('timer:update', (event, timeLeft) => {
  tray.setTitle(timeLeft);
});

ipcMain.on('app:close', (event, timeLeft) => {
  tray.setTitle(timeLeft);
});