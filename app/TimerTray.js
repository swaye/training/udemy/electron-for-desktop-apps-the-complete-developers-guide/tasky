const electron = require('electron');
const { Tray, app, Menu } = electron;

class TimerTray extends Tray {

  constructor({icon, mainWindow}) {
    super(icon);
    this.mainWindow = mainWindow;
    this.setToolTip('Timer App');
    this.on('click', this.onClick.bind(this));
    this.on('right-click', this.onRightClick.bind(this));
    
  }

  onClick = (event, bounds) => {
    if (this.mainWindow.isVisible()) {
        this.mainWindow.hide();
    } else {
      this.setBounds(bounds);
      this.mainWindow.show();
    }
  };

  onRightClick = () => {
    const menuConfig = Menu.buildFromTemplate([
      {
        label: 'Quit',
        click: () => app.quit()
      }
    ]);

    this.popUpContextMenu(menuConfig);
  };

  setBounds = (bounds) =>{
    const { height, width } = this.mainWindow.getBounds();
    const { x, y } = bounds;
    const xPos =  (x - width / 2);
    const yPos = process.platform === 'darwin' 
      ? y 
      : y - height;
    this.mainWindow.setBounds({
      x: xPos,
      y: yPos,
      height,
      width
    })
  };
}

module.exports = TimerTray;